<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('primer_apellido',20);
            $table->string('segundo_apellido',20);
            $table->string('primer_nombre',20);
            $table->string('segundo_nombre',50)->nullable();
            $table->string('numero_identificacion',20)->unique();
            $table->string('email',300)->unique();
            $table->date('fecha_ingreso');
            $table->enum('estado', ['activo', 'inactivo'])->default('activo');
            $table->foreignId('country_id')->constrained();
            $table->foreignId('area_id')->constrained();
            $table->foreignId('document_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

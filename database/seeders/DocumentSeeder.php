<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documents')->insert([
            [
                'nombre' => 'Cédula de Ciudadanía',
                'abreviatura' => 'CC'
            ],
            [
                'nombre' => 'Cédula de Extranjería',
                'abreviatura' => 'CE'
            ],
            [
                'nombre' => 'Pasaporte',
                'abreviatura' => 'PA'
            ],
            [
                'nombre' => 'Permiso Especial',
                'abreviatura' => 'PE'
            ],

        ]);
    }
}

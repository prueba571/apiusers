<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            [
                'nombre' => 'Administración',
            ],
            [
                'nombre' => 'Financiera',
            ],
            [
                'nombre' => 'Compras',
            ],
            [
                'nombre' => 'Infraestructura',
            ],
            [
                'nombre' => 'Operación',
            ],
            [
                'nombre' => 'Talento Humano',
            ],
            [
                'nombre' => 'Servicios Varios',
            ],
        ]);
    }
}

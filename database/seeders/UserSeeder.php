<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'primer_nombre' => 'Destiny',
                'segundo_nombre' => null,
                'primer_apellido' => 'Hope',
                'segundo_apellido' => 'Cyrus',
                'numero_identificacion' => '123456',
                'email' => 'destiny.hope@cidenet.co',
                'fecha_ingreso' => '2022/01/03',
                'estado' => 'activo',
                'country_id' => 1,
                'document_id' => 1,
                'area_id' => 1,
            ],
            [
                'primer_nombre' => 'Stefani',
                'segundo_nombre' => 'Corradini ',
                'primer_apellido' => 'Angelina',
                'segundo_apellido' => 'Germanotta',
                'numero_identificacion' => '123457',
                'email' => 'stefani.corradini@cidenet.co',
                'fecha_ingreso' => '2021/12/26',
                'estado' => 'activo',
                'country_id' => 2,
                'document_id' => 3,
                'area_id' => 3,
            ],
            [
                'primer_nombre' => 'Marshall',
                'segundo_nombre' => 'Bruce',
                'primer_apellido' => 'Mathers',
                'segundo_apellido' => 'Arce',
                'numero_identificacion' => '123458',
                'email' => 'marshall.mathers@cidenet.co',
                'fecha_ingreso' => '2022/05/15',
                'estado' => 'activo',
                'country_id' => 1,
                'document_id' => 2,
                'area_id' => 4,
            ],

        ]);
    }
}

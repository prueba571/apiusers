<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

use App\Models\User;


class UserController extends Controller
{


    private $status = 'success';
    private $code = 200;
    private $message = 'Consulta exitosa!';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = User::with(
                array(
                    'country'=>function($query){
                            $query->select('nombre','id');
                    },
                    'area'=>function($query){
                        $query->select('nombre','id');
                    },
                    'document'=>function($query){
                        $query->select('nombre','id');
                    },
                )
            )->get();
            return Request::api($this->status,$users, $this->message,$this->code);
        } catch (\Throwable $th) {
            //dd($th->getMessage());
            return Request::api('error',[],report($th),500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dateNow = Carbon::now();
        $dateInitial = Carbon::now()->subMonth();//restar un mes a la fecha actual.

        //construir emial
        $emailUserName = $request->primer_nombre.'.'.$request->primer_apellido;
        $email = $this->createEmail($emailUserName);
        $request->email = $email;
        //Validar campos
        $validator = $this->validateRequest($request);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }

        try {
            $user = User::create($request->all()+['email'=>$email]);
            return Request::api($this->status,$user, 'Registro creado con éxito!',$this->code);
        } catch (\Throwable $th) {
            dd($th->getMessage());
            return Request::api('error',[],report($th),500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::where('id',$id)->with(
                array(
                    'country'=>function($query){
                            $query->select('nombre','id');
                    },
                    'area'=>function($query){
                        $query->select('nombre','id');
                    },
                    'document'=>function($query){
                        $query->select('nombre','id');
                    },
                )
            )->first();

            if(!isset($user)){
                $this->status = 'error';
                $this->code = 409;
                $this->message = 'El registro que intenta consultar no existe!';
            }
            return Request::api($this->status,$user,$this->message,$this->code);
        } catch (\Throwable $th) {dd($th->getMessage());
            return Request::api('error',[],report($th),500);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id',$id)->first();
        $name = null;
        $apellido = null;
        $email = null;

        if(!isset($user)){
            return Request::api('error',[],'El usuario que intenta actualizar no existe.', 409);
        }else{
            $name = $user->primer_nombre;
            $lastName = $user->primer_apellido;
        }

        //construir email si es neceario
        if($request->primer_nombre !== $name || $request->primer_apellido !== $lastName ){
            $emailUserName = (isset($request->primer_nombre)?$request->primer_nombre:$name).'.'.( isset($request->primer_apellido)?$request->primer_apellido:$lastName);
            $email = $this->createEmail($emailUserName);
            $request->email = $email;
        }
       // dd($request->primer_nombre , $name, $request->primer_apellido , $lastName);
        //validar campos
        $validator = $this->validateRequest($request,$user->id,true);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }

        try {
            isset($email) ? $user->update($request->all()+['email'=>$email]):$user->update($request->all());
            $this->message = 'Registro actualizado con éxito';
            return Request::api($this->status,[], $this->message, $this->code);
        } catch (\Throwable $th) {
            return ($th->getMessage());
            return Request::api('error',[],report($th),500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::destroy($id);
            $this->message = 'Usuario eliminado con éxito!';
            if(!$user){
                $this->status = 'error';
                $this->code = 409;
                $this->message = 'El registro que intenta eliminar no existe!';
            }
            return Request::api($this->status,[], $this->message,$this->code);

        } catch (\Throwable $th) {
            return Request::api('error',[],report($th),500);
        }
    }


    /**
     * Validate fields
     */
    private function validateRequest($request,$user_id='',$edit=false){

        $dateNow = Carbon::now();
        $dateInitial = Carbon::now()->subMonth();//restar un me a la fecha actual.
        $required = $edit ? '':'required';
        $regexSegundoNombre = isset($request->segundo_nombre) ? `regex:/^[a-zA-Z\s]+$/` : '';


        $validator = Validator::make($request->all(),[
            'primer_nombre'=>$required.'|max:20|regex:/^[a-zA-Z]+$/',
            'segundo_nombre'=>'max:50|'.$regexSegundoNombre,
            'primer_apellido'=>$required.'|max:20|regex:/^[a-zA-Z]+$/',
            'segundo_apellido'=>$required.'|max:20|regex:/^[a-zA-Z]+$/',
            'numero_identificacion'=>$required.'|max:20|unique:users,numero_identificacion,'.$user_id.' |regex:/^[a-zA-Z0-9]+$/',
            //'email'=>'required|string|email|max:300|unique:users,email,'.$user_id,
            'fecha_ingreso'=>$required.'|date|after_or_equal:'.$dateInitial.'|before_or_equal:'.$dateNow,
            'estado'=>$required.'|in:activo',
            'country_id'=>$required.'|integer|',
            'area_id'=>$required.'|integer',
            'document_id'=>$required.'|integer',
        ]);

        return $validator;
    }

     /**
     * Create unique email for the user
     */
    private function createEmail($emailUserName){

        function consultarEmail($email){
            $temporaryEmail = User::select('email')->where('email', $email)->first();//dd($existUserserEmail,$email);
            return $temporaryEmail;
        }

        $emailDomain = '@cidenet.com.co';
        $temporaryEmail =  strtolower($emailUserName).$emailDomain;
        $count = 0;
        while(isset($temporaryEmail)){
            $aux = strtolower($emailUserName . ($count == 0 ? '': $count) . $emailDomain);
            $temporaryEmail = consultarEmail($aux);
            $email=$aux;
            $count++;
        }

        return $email;
    }
}

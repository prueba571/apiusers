<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Definiendo respuesta de la api a nivel global
        Request::macro('api', function ($status,$data, $message, $code = 404) {
            $message = $message == '' ? 'Hubo un error, por favor intente nuevamente' : $message;
            $response = [
                'status' => $status,
                'data' => $data,
                'message' => $message
            ];
            return response()->json($response, $code);
        });
    }
}
